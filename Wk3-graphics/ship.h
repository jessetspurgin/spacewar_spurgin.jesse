#ifndef _SHIP_H
#define _SHIP_H
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace shipNS
{
	const int	WIDTH = 32;		// width of the ship image
	const int	HEIGHT = 32;		// hight of the ship image
	const int	X = GAME_WIDTH/2 - WIDTH/2;
	const int	Y = GAME_HEIGHT/2 - HEIGHT/2;

	const float SPEED = 200.0f; // set speet for the ship
	const float MASS = 300.0f;
	const float ROTATION_RATE = (float)PI/4;

	// realated to the spritesheet, used by RECT
	const int	TEXTURE_COLS = 8;		// ship texture has two
	//ship realated constants
	const int	SHIP1_START_FRAME = 0;		// starting frame of animation for ship 1
	const int	SHIP1_END_FRAME = 3;		// ending frame of animation for ship 1 (0,1,2,3)
	const int	SHIP2_START_FRAME = 8;		// starting frame of animation for ship 2
	const int	SHIP2_END_FRAME = 11;		// ending frame of animation for ship 2 (8,9,10,11)
	const float SHIP_ANIMATION_DELAY = 0.5f;		// between frame of ship animation

	const int	SHIELD_START_FRAME = 24;		
	const int	SHIELD_END_FRAME = 27;		// frames 24,25,26,27
	const float SHIELD_ANIMATION_DELAY = 0.3f;		//shiled animation

}

class Ship : public Entity
{
private:
	bool shieldOn; // store if the shiled is on
	Image shield;  // image for the shiled

public:
	Ship(); // constructor

	void update( float frameTime );

	virtual void draw();
	virtual bool initialize( Game *gamePtr, int width, int height, int ncols, TextureManager *texturePtr );
	
	void damage( WEAPON );

};

#endif