// 4.4 The Space War Class
// Chapter 4 page 103

// Defineing the spacewar class
#include "spacewar.h"
#include <sstream>

// Constructor
Spacewar::Spacewar()
{

}

// Destructor
Spacewar::~Spacewar()
{
releaseAll(); // Call onLostDevice() for every graphics item
}


// Initializes the game
// Throws GameError on error
void Spacewar::initialize(HWND hwnd)
{
	Game::initialize(hwnd); // Call Games's initialize

	if( !nebulaTexture.initialize( graphics, NEBULA_IMAGE ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR,
			"Error initializing nebula texture" ) );
	}
	
	if( !spaceTexture.initialize( graphics, SPACE_IMAGE ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR,
			"Error initializing space texture" ) );
	}

	if( !gameTextures.initialize( graphics, TEXTURES_IMAGE ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR,
			"Error initializing games textures" ) );
	}
	
	//initialize the Images
	if( !nebula.initialize( graphics, 0, 0, 0, &nebulaTexture ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR,
			"Error initializing nebula image" ) );
	}
	
	if( !space.initialize( graphics, 0, 0, 0, &spaceTexture ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR,
			"Error initializing space image" ) );
	}
	space.setScale(2);
	space.setX( GAME_WIDTH - space.getWidth() * 1.5f );
	space.setY( GAME_HEIGHT - space.getHeight() * 1.5f );

	if( !planet.initialize( this, planetNS::WIDTH, planetNS::HEIGHT, planetNS::TEXTURE_COLS, &gameTextures ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR,
			"Error initializing planet" ) );
	}

	//place the planet in the center of the screen
	planet.setX( GAME_WIDTH*0.5f - planet.getWidth()*0.5f );
	planet.setY( GAME_HEIGHT*0.5f - planet.getHeight()*0.5f );
	
	//Asteroid 1
	if( !asteroid1.initialize( this, asteroidNS::WIDTH, asteroidNS::HEIGHT, asteroidNS::TEXTURE_COLS, &gameTextures ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR,
			"Error initializing asteriod" ) );
	}
	asteroid1.setFrames( asteroidNS::START_FRAME_1, asteroidNS::END_FRAME_1);
	asteroid1.setCurrentFrame( asteroidNS::START_FRAME_1 );
	
	asteroid1.reset();
	asteroid1.setVelocity( VECTOR2( -50.0f, 0 ) );
	
	//Asteroid 2
	if( !asteroid2.initialize( this, asteroidNS::WIDTH, asteroidNS::HEIGHT, asteroidNS::TEXTURE_COLS, &gameTextures ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR,
			"Error initializing asteriod" ) );
	}
	asteroid2.setFrames( asteroidNS::START_FRAME_2, asteroidNS::END_FRAME_2);
	asteroid2.setCurrentFrame( asteroidNS::START_FRAME_2 );
	
	asteroid2.reset();
	asteroid2.setVelocity( VECTOR2( -75.0f, 0 ) );
	
	//Asteroid 3
	if( !asteroid3.initialize( this, asteroidNS::WIDTH, asteroidNS::HEIGHT, asteroidNS::TEXTURE_COLS, &gameTextures ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR,
			"Error initializing asteriod" ) );
	}
	asteroid3.setFrames( asteroidNS::START_FRAME_3, asteroidNS::END_FRAME_3);
	asteroid3.setCurrentFrame( asteroidNS::START_FRAME_3 );
	
	asteroid3.reset();
	asteroid3.setVelocity( VECTOR2( -100.0f, 0 ) );

	//Ship 1
	if( !ship1.initialize( this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &gameTextures ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR,
			"Error initializing ship image" ) );
	}
	//starting screen position for the ship
	ship1.setX( GAME_WIDTH/4 );
	ship1.setY( GAME_HEIGHT/4 );

	// turns on the animaiton (lets Image know this image is animated)
	ship1.setFrames( shipNS::SHIP1_START_FRAME, shipNS::SHIP1_END_FRAME );	// set up the animation frames
	ship1.setCurrentFrame( shipNS::SHIP1_START_FRAME ); // starting frame
	ship1.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY ); // how long to show each frame

	//ship1.setVelocity( VECTOR2( shipNS::SPEED, -shipNS::SPEED) );

	//Ship2
	if( !ship2.initialize( this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &gameTextures ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR,
			"Error initializing ship image" ) );
	}
	//starting screen position for the ship
	ship2.setX( GAME_WIDTH*3/4 );
	ship2.setY( GAME_HEIGHT*3/4 );

	// turns on the animaiton (lets Image know this image is animated)
	ship2.setFrames( shipNS::SHIP2_START_FRAME, shipNS::SHIP2_END_FRAME );	// set up the animation frames
	ship2.setCurrentFrame( shipNS::SHIP2_START_FRAME ); // starting frame
	ship2.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY ); // how long to show each frame

	ship2.setVelocity( VECTOR2( shipNS::SPEED, -shipNS::SPEED) );

	return;
}


void Spacewar::update()	// Update all game items
{
	//Game Timer
	gameTimer -= frameTime;
	if (gameTimer <= 0.0f)
	{
		gameOver = true;
		paused = true;
	}

	// makes the ship attracted to the planet
	ship1.gravityForce( &planet, frameTime );
	ship2.gravityForce( &planet, frameTime );

	// animate our ship and planet
	ship1.update(frameTime);
	ship2.update(frameTime);
	planet.update(frameTime);
	space.update(frameTime);
	asteroid1.update(frameTime);
	asteroid2.update(frameTime);
	asteroid3.update(frameTime);

	/*				Week 6 testing fun stuff
	// rotate the ship
	ship.setDegrees( ship.getDegrees() + frameTime * 90.0f );

	// scale the ship
	ship.setScale( ship.getScale() - frameTime * 0.2f );

	// move the ship around
	ship.setX( ship.getX() + frameTime * 100.0f );

	if( ship.getX() > GAME_WIDTH ) // if ship goes off screen
	{
		ship.setX ( (float) - ship.getWidth() ); // position offscreen left
		ship.setScale( 1.5f ); // reset the ship scale to 1.5
	}
	*/
	
	//Movement Keys
	if (input->isKeyDown(SHIP_RIGHT_KEY) || input->isKeyDown(D_KEY))

	{
		//if(ship1.getX() < (GAME_WIDTH - ship1.getWidth()))
		//{
		//ship1.setX(ship1.getX() + frameTime * shipNS::SPEED);
		if( ship1.getVelocity().x < shipNS::SPEED ) 
		{
			ship1.setVelocity( VECTOR2(ship1.getVelocity().x + frameTime * shipNS::SPEED, ship1.getVelocity().y ) );
		}
	
		//}
	}

	if (input->isKeyDown(SHIP_LEFT_KEY) || input->isKeyDown(A_KEY))
	{
		//if(ship1.getX() > 0)
		//{
		//ship1.setX(ship1.getX() - frameTime * shipNS::SPEED);
		//}
		if( ship1.getVelocity().x > -shipNS::SPEED ) 
		{
			ship1.setVelocity( VECTOR2( ship1.getVelocity().x - frameTime * shipNS::SPEED, ship1.getVelocity().y ) );
		}
	}
	
	if (input->isKeyDown(SHIP_UP_KEY) || input->isKeyDown(W_KEY))
	{

		//ship1.setVelocity(VECTOR2(x,y));// += frameTime * shipNS::SPEED;
		//ship1.setX(ship1.getX() + frameTime * velocity.x); // Apply X velocity
		//if(ship1.getY() > 0)
		//{
		//ship1.setY(ship1.getY() - frameTime * shipNS::SPEED);
		//}
		if( ship1.getVelocity().y > -shipNS::SPEED )
		{
			ship1.setVelocity( VECTOR2( ship1.getVelocity().x, ship1.getVelocity().y - frameTime * shipNS::SPEED ) );
		}
		//if((input->isKeyDown(E_KEY)))
		//{
		//	ship1.setVelocity( VECTOR2( cos(ship1.getDegrees()*PI/180) + frameTime * shipNS::SPEED, sin((ship1.getDegrees()*PI/180) + frameTime * shipNS::SPEED)));
		//}
		//if((input->isKeyDown(Q_KEY)))
		//{
		//	ship1.setVelocity( VECTOR2( cos(ship1.getDegrees()*PI/180) + frameTime * shipNS::SPEED, sin((ship1.getDegrees()*PI/180) + frameTime * shipNS::SPEED)));
		//}
	}

	if( input->isKeyDown(SHIP_DOWN_KEY) || input->isKeyDown(S_KEY))
	{
		//if( ship1.getY() < (GAME_HEIGHT - ship1.getWidth()))
		//{
		//ship1.setY(ship1.getY() + frameTime * shipNS::SPEED);
		//}
		if( ship1.getVelocity().y < shipNS::SPEED )
		{
			ship1.setVelocity( VECTOR2( ship1.getVelocity().x, ship1.getVelocity().y + frameTime * shipNS::SPEED ) );
		}
	}

	//Rotation Keys for movement
	if (input->isKeyDown(Q_KEY))
	{
		ship1.setDegrees(ship1.getDegrees() - frameTime * 180.0f);
	}
	
	if (input->isKeyDown(E_KEY))
	{
		ship1.setDegrees(ship1.getDegrees() + frameTime * 180.0f);
	}

	space.setDegrees(space.getDegrees() + frameTime * 1.0f);

	planet.setDegrees(planet.getDegrees() + frameTime * 5.0f);

	if( ship1.getVelocity().x < (frameTime * shipNS::SPEED)*0.5 && ship1.getVelocity().x > (frameTime * shipNS::SPEED)*-0.5 )
	{
		ship1.setVelocity( VECTOR2(0, ship1.getVelocity().y) );
	}
	if( ship1.getVelocity().y < (frameTime * shipNS::SPEED)*0.5 && ship1.getVelocity().y > (frameTime * shipNS::SPEED)*-0.5 )
	{
		ship1.setVelocity( VECTOR2(ship1.getVelocity().x,0) );
	}

	std::stringstream ss;
	ss << "Score: " << score;
	scoreOn = ss.str();
}


void Spacewar::ai()	// Artificial Intelligence
{}


void Spacewar::collisions()	// Handle collisions
{
	VECTOR2 collisionVector;
	if( ship1.collidesWith( planet, collisionVector ) )
	{
		//bounce off the planet
		ship1.bounce( collisionVector, planet );
		ship1.damage( PLANET );
	}
	if( ship2.collidesWith( planet, collisionVector ) )
	{
		//bounce off the planet
		ship2.bounce( collisionVector, planet );
		ship2.damage( PLANET );
	}
	if( ship1.collidesWith( ship2, collisionVector ) )
	{
		ship1.bounce( collisionVector, ship2 );
		ship1.damage( SHIP );

		ship2.bounce( collisionVector * -1, ship1 );
		ship2.damage( SHIP );
	}

	if( ship1.collidesWith( asteroid1, collisionVector) )
	{
		ship1.damage( ASTEROID );
		asteroid1.setActive(false);
		asteroid1.setVisible(false);
		asteroid1.setExplode(true);
		score++;
	}

	if( ship1.collidesWith( asteroid2, collisionVector) )
	{
		ship1.damage( ASTEROID );
		asteroid2.setActive(false);
		asteroid2.setVisible(false);
		asteroid2.setExplode(true);
		score++;
	}

	if( ship1.collidesWith( asteroid3, collisionVector) )
	{
		ship1.damage( ASTEROID );
		asteroid3.setActive(false);
		asteroid3.setVisible(false);
		asteroid3.setExplode(true);
		score++;
	}
}


void Spacewar::render()	// Render game items
{
	graphics->spriteBegin();
	//call draw functions after!
	//ORDER IS IMPORTANT! background is drawn first, foreground is drawn second
	
	nebula.draw(); //under the planet
	space.draw();
	planet.draw(); //over the nebula
	asteroid1.draw();
	asteroid2.draw();
	asteroid3.draw();
	ship1.draw(); //over the planet
	ship2.draw(); //over the planet
	
	//fall draw functions before!
	graphics->spriteEnd();
}

// The graphics device was lost
// Release all reserved video memory so graphics device may be reset
void Spacewar::releaseAll()	
{
	gameTextures.onLostDevice();
	nebulaTexture.onLostDevice();
	Game::releaseAll();

return;
}

// The grahics device has been reset
// Recreate all surfaces
void Spacewar::resetAll()	
{
	nebulaTexture.onResetDevice();
	gameTextures.onResetDevice();
	Game::resetAll();

return;
}