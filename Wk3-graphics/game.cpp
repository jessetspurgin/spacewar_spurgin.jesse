//game.cpp file

#include "game.h"

// Constructor
Game::Game()
{
	input = new Input();
	paused = false;		//game is not paused
	graphics = NULL;
	initialized = false;

	console = NULL;
	fps = 100;
	fpsOn = false;		//default is show fps off
	score = 0;
	gameTimer = 120.0f;
	gameOver = false;
}

// Destructor
Game::~Game() // "~" how C++ knows what the destructor is
{
	deleteAll();		//free all reserved memory
	ShowCursor(true);	//show the cursor
}

//Message Handler
LRESULT	Game::messageHandler( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	if( initialized )
	{
		switch ( msg )
		{
			case WM_DESTROY:
				PostQuitMessage(0); //tell WindowsOS to kill this program 
				return 0;
			case WM_KEYDOWN: case WM_SYSKEYDOWN:
				input->keyDown(wParam);
				return 0;
			case WM_KEYUP: case WM_SYSKEYUP:
				input->keyUp(wParam);
				return 0;
			case WM_CHAR:
				input->keyIn(wParam);
				return 0;
			case WM_MOUSEMOVE:
				input->mouseIn( lParam );
				return 0;
			case WM_INPUT:
				input->mouseRawIn( lParam );
				return 0;
			case WM_LBUTTONDOWN:	//left mouse down
				input->setMouseLButton(true);
				input->mouseIn(lParam);
				return 0;
			case WM_LBUTTONUP:	//left mouse up
				input->setMouseLButton(true);
				input->mouseIn(lParam);
				return 0;
			case WM_MBUTTONDOWN:	//middle mouse down
				input->setMouseMButton(true);
				input->mouseIn(lParam);
				return 0;
			case WM_MBUTTONUP:	//middle mouse up
				input->setMouseMButton(true);
				input->mouseIn(lParam);
				return 0;
			case WM_RBUTTONDOWN:	//right mouse down
				input->setMouseRButton(true);
				input->mouseIn(lParam);
				return 0;
			case WM_RBUTTONUP:	//right mouse up
				input->setMouseRButton(true);
				input->mouseIn(lParam);
				return 0;
			case WM_DEVICECHANGE:	//chech for controllers
				input->checkControllers();
				return 0;
		}
	}
	return DefWindowProc( hwnd, msg, wParam, lParam );	//let WindowsOS handle it
}

//Initilize Fundtion
void Game::initialize( HWND hw )
{
	hwnd = hw;		//save window handle
	//initialize graphics
	graphics = new Graphics();
	graphics->initialize( hwnd, GAME_WIDTH, GAME_HEIGHT, FULLSCREEN );

	//initialize the console
	console = new Console();
	console->initialize(graphics, input);
	console->print("---Console---");	//presp the console

	//initialize directX font
	if(dxFont.initialize(graphics, gameNS::POINT_SIZE, false, false, gameNS::FONT) == false)
	{
		throw(GameError(gameErrorNS::FATAL_ERROR, "Failed to initialize DirectX Font"));
	}
	dxFont.setFontColor(gameNS::FONT_COLOR);

	//initialize directX font2
	if (dxFont2.initialize(graphics, 48, false, false, gameNS::FONT) == false)
	{
		throw(GameError(gameErrorNS::FATAL_ERROR, "Failed to initialize DirectX Font"));
	}
	dxFont2.setFontColor(SETCOLOR_ARGB(255, 255, 50, 50));

	//initialize input, do not capture mouse
	input->initialize( hwnd, false );

	// Set up high resolution timer
	if( QueryPerformanceFrequency( &timerFreq ) == false )
	{
		throw( GameError( gameErrorNS::FATAL_ERROR, "Error initializing high res timer" ) );
	}
	QueryPerformanceCounter( &timeStart );	// Get starting time
	
	// Initialization completed!
	initialized = true;
}

void Game::handleLostGraphicsDevice()
{
	// Test for and handle lost device
	hr = graphics->getDeviceState();
	if( FAILED(hr) ) // If the graphics device is not in a valid state
	{
		if( hr == D3DERR_DEVICELOST ) // If the device is lost and not available for reset
		{
			Sleep( 100 );
			return;
		}
		else if( hr == D3DERR_DEVICENOTRESET ) // If the device was lost but now is available for reset
		{
			releaseAll();
			hr = graphics->reset(); // Attempt to reset graphics device
			if( FAILED(hr) ) return; // If reset failed, return
			resetAll();
		}
		else
		{
			return; // Due to other device error
		}
	}
}

// Render game items
void Game::renderGame()
{
	const int BUF_SIZE = 20;
	static char buffer[BUF_SIZE];

	// Start rendering
	if (SUCCEEDED(graphics->beginScene()))
	{
		// Render is a pure virtual function that must be provided in the inheriting class
		render();	// Call render in derived class

		graphics->spriteBegin();
		if(fpsOn)
		{
			_snprintf_s(buffer, BUF_SIZE, "fps %d", (int)fps);
			dxFont.print(buffer, GAME_WIDTH - 100, GAME_HEIGHT - 28);
		}
		
		dxFont.print( scoreOn, 50, GAME_HEIGHT - 28);
		
		//Displaying gameTimer
		const int BUF_SIZE2 = 20;
		static char buffer2[BUF_SIZE2];
		_snprintf_s(buffer2, BUF_SIZE2, "Timer %d", (int)gameTimer);
		dxFont.print( buffer2, 285, GAME_HEIGHT - 470);
		if (gameOver)
		{
			dxFont2.print("GAME OVER", 285, 240);
		}

		graphics->spriteEnd();

		console->draw();		//console will be on top

		graphics->endScene();	// Stop rendering
	}
	handleLostGraphicsDevice();

	graphics->showBackBuffer();	// Display the backbuffer on the screen
}

// Call repeatedly by the main message loop in WinMain

void Game::run(HWND hwnd)
{
	if(graphics == NULL) // If graphics not initialized
	return;
	// Calculate elapsed time of last frame, save in frameTime
	QueryPerformanceCounter(&timeEnd);
	frameTime = (float)(timeEnd.QuadPart - timeStart.QuadPart ) /
	(float)timerFreq.QuadPart;
	// Power-saving code, requires winmm.lib
	// If not enough time has elapsed for desired frame rate
	if (frameTime < MIN_FRAME_TIME)
	{
		sleepTime = (DWORD)((MIN_FRAME_TIME - frameTime)*1000);
		timeBeginPeriod(1); //request 1mS resolution for windows timer
		Sleep(sleepTime); //release CPU for sleepTime
		timeEndPeriod(1); //end 1mS timer resolution
		return;
	}
	if (frameTime > 0.0)
		fps = (fps*0.99f) + (0.01f/frameTime); //average fps
	if (frameTime > MAX_FRAME_TIME) // If frame rate is very slow
		frameTime = MAX_FRAME_TIME; // Limit maximum frameTime
	timeStart = timeEnd;
	input->readControllers(); // Read state of controllers
	// update(), ai(), and collisions() are pure virtual functions.
	// These functions must be provided in the class that inherits from
	// Game.
	if (!paused) // If not paused
	{
		update(); // Update all game items
		ai(); // Artificial intelligence
		collisions(); // Handle collisions
		input->vibrateControllers(frameTime);// Handle controller vibration
	}
	renderGame(); // Draw all game items

	//check for console Key
	if(input->wasKeyPressed(CONSOLE_KEY))
	{
		console->showHide();
		paused = console->getVisible();		//pause when visible
	}
	consoleCommand();		//process user entered console command
	
	input->readControllers();		//read sate of controllers

	// Clear input
	// Call this after all key checks are done
	input->clear(inputNS::KEYS_PRESSED);
}
//Process console Commands
//override this function in the derived class if new console commands are entered
void Game::consoleCommand()
{
	command = console->getCommand();		//get command from console
	if(command == "")		//if no command
		return;
	
	//process for adding console command
	if(command == "help")
	{
		console->print("Console Commands:");
		console->print("fps - toggle display of frames per second");
		console->print("exit - to exit game");
		return;
	}

	if(command == "fps")
	{
		fpsOn = !fpsOn;
		if(fpsOn)
			console->print("fps On");
		else
			console->print("fps Off");
	}

	if (command == "exit")
	{
		PostQuitMessage(0);
	}
}

void Game::releaseAll()
{
	//order matters
	SAFE_ON_LOST_DEVICE(console);
	dxFont.onLostDevice();
	return;
}

void Game::resetAll()
{
	//order matters
	dxFont.onResetDevice();
	SAFE_ON_RESET_DEVICE(console);
	return;
}

void Game::deleteAll()
{
	releaseAll();
	SAFE_DELETE(graphics);
	SAFE_DELETE(input);
	SAFE_DELETE(console);
	initialized = false;
}