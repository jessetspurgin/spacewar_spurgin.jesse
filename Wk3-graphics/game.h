//Game Engine
//Game.h


//prevents errors when redefining this class
#ifndef _GAME_H  //prevent multiple deffinitions of this class
#define _GAME_H

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <MMSystem.h>
#include "graphics.h"
#include "constants.h"
#include "gameError.h"
#include "input.h"
#include "console.h" //new
#include "textDX.h" //new
#include <string>

namespace gameNS
{
	const char FONT[] = "Courier New";		//console font
	const int POINT_SIZE = 14;				//how big the font will be
	const COLOR_ARGB FONT_COLOR = SETCOLOR_ARGB(255,255,255,255);	//which is white
}

class Game
{
protected:
	//common game properties
	Graphics* graphics;				//pointer to Graphics
	Input* input;					//
	Console* console;				//
	HWND  hwnd;						//window handle
	HRESULT hr;						//return type from Windows/DirectX
	LARGE_INTEGER timeStart;		//performance counter start value
	LARGE_INTEGER timeEnd;			//performance counter end value
	LARGE_INTEGER timerFreq;		//performance counter frequency
	float frameTime;				//time required for last frame
	float fps;						//frames per second
	DWORD sleepTime;				//number of milliseconds fo sleep between frames
	bool paused;					//true if game paused
	bool initialized;				//true if initilized
	std::string scoreOn;
	int score;
	bool fpsOn;						//true when displaying FPS
	TextDX dxFont;					//
	TextDX dxFont2;					// GameOver font
	std::string command;			//
	float gameTimer;				//GameTimer
	bool gameOver;

public:

	//constructor
	Game();
	//destructor
	virtual ~Game();

	//***Member Functions***

	//window message handler
	LRESULT	messageHandler( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );

	//initialize the game
	virtual void initialize( HWND hwnd );

	//call run repeatedly by the main message loop in WinMain
	virtual void run( HWND );

	//call when the graphics device is lost
	virtual void releaseAll();

	//recreate all surfaces and reset all enttities
	virtual void resetAll();

	//delete all reserved memory
	virtual void deleteAll();

	//render game items - drawing sprites
	virtual void renderGame();

	//handle lost graphics device
	virtual void handleLostGraphicsDevice();

	//returen pointer fo graphics
	Graphics* getGraphics() { return graphics; }

	//retern pointer to graphics
	Input* getInput() { return input; }

	//exit the game
	void exitGame() { PostMessage( hwnd, WM_DESTROY, 0, 0 ); }

	//to process console commands
	virtual void consoleCommand();

	//***pure virtural function declaration
	//update game items
	virtual void update() = 0;

	//preform Artifitial Intelligence calculations
	virtual void ai() = 0;

	//check for collisions
	virtual void collisions() = 0;

	//render graphics
	virtual void render() = 0;

};
#endif  //all the way at the end of the code of this file