// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 planet.h v1.0

#ifndef _ASTEROID_H               // Prevent multiple definitions if this 
#define _ASTEROID_H               // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace asteroidNS
{
    const int   WIDTH = 32;                // image width
    const int   HEIGHT = 32;               // image height
    const int   COLLISION_RADIUS = 32/2;   // for circular collision
    const int   X = GAME_WIDTH/2 - WIDTH/2; // location on screen
    const int   Y = GAME_HEIGHT/2 - HEIGHT/2;
    const float MASS = 500.0f;             // mass
    const int   TEXTURE_COLS = 8;           // texture has 1 column
    const int   START_FRAME_1 = 48;            // starts at frame 0
    const int   END_FRAME_1 = 48;              // no animation
	const int   START_FRAME_2 = 49;            // starts at frame 0
    const int   END_FRAME_2 = 49;              // no animation
	const int   START_FRAME_3 = 50;            // starts at frame 0
    const int   END_FRAME_3 = 50;              // no animation
	const int   EXPLOSION_START_FRAME = 32; // explosion start frame
    const int   EXPLOSION_END_FRAME = 39;   // explosion end frame
    const float EXPLOSION_ANIMATION_DELAY = 0.2f;   // time between frames
	
	const float ROTATION_RATE = (float)PI/4;
	const float RESPAWN_IN	= 1.0f;
}

class Asteroid : public Entity // inherits from Entity class
{
private:
	float respawn_timer;
	bool waiting;
	Image explosion;
	bool explode;

public:
    // constructor
    Asteroid();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
	void draw();
	void update( float frameTime );
	void reset();
	void setExplode(bool val) { explode = val; }
};
#endif

