#include "ship.h"

//constructor
Ship::Ship() : Entity()
{
	spriteData.width = shipNS::WIDTH;
	spriteData.height = shipNS::HEIGHT;
	spriteData.x = shipNS::X;
	spriteData.y = shipNS::Y;
	spriteData.rect.bottom = shipNS::HEIGHT;
	spriteData.rect.right = shipNS::WIDTH;

	frameDelay = shipNS::SHIP_ANIMATION_DELAY;
	startFrame = shipNS::SHIP1_START_FRAME;
	endFrame = shipNS::SHIP1_END_FRAME;
	currentFrame = startFrame;

	collisionType = entityNS::CIRCLE;
	radius = shipNS::WIDTH/2.0f;

	velocity.x = 0;
	velocity.y = 0;
}

//========================================================================
// Update
// Typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//========================================================================
void Ship::update ( float frameTime )
{
	Entity::update ( frameTime );

	//spriteData.angle += frameTime * shipNS::ROTATION_RATE; // Rotate the ship
	
	spriteData.x += frameTime * velocity.x; // Move ship along X
	spriteData.y += frameTime * velocity.y; // Move ship along Y
	// Bounce off walls
	// If hit rightscreen edge
	if( spriteData.x > GAME_WIDTH-shipNS::WIDTH * getScale() )
	{
		// Position at right screen edge
		spriteData.x = GAME_WIDTH-shipNS::WIDTH * getScale();
		velocity.x = -velocity.x;// Reverse X direction
	}
	else if (spriteData.x < 0) // Else if hit left screen edge
	{
		spriteData.x = 0; // Position at left screen edge
		velocity.x = -velocity.x; // Reverse X direction
		// If hit bottom screen edge
	}
		if ( spriteData.y > GAME_HEIGHT-shipNS::HEIGHT * getScale() )
	{
		// Position at bottom screen edge
		spriteData.y = GAME_HEIGHT-shipNS::HEIGHT * getScale(); 
		velocity.y = -velocity.y; // Reverse Y direction
	}
	else if (spriteData.y < 0) // Else if hit top screen edge
	{
		spriteData.y = 0; // Position at top screen edge
		velocity.y = -velocity.y; // Reverse Y direction
	}

	if( shieldOn )
	{
		shield.update( frameTime );
		if( shield.getAnimationComplete() )
		{
			shieldOn = false;
			shield.setAnimationComplete( false ); //reset animation
			shield.setCurrentFrame( shipNS::SHIELD_START_FRAME );
		}
	}
}

bool Ship::initialize( Game *gamePtr, int width, int height, int ncols, TextureManager *texturePtr )
{
	shield.initialize( gamePtr->getGraphics(), width, height, ncols, texturePtr );
	shield.setFrames( shipNS::SHIELD_START_FRAME, shipNS::SHIELD_END_FRAME );
	shield.setCurrentFrame( shipNS::SHIELD_START_FRAME );
	shield.setFrameDelay( shipNS::SHIELD_ANIMATION_DELAY );
	shield.setLoop( false ); //we dont want the animation to loop, play only once
	return( Entity::initialize( gamePtr, width, height, ncols, texturePtr ) );
}

void Ship::draw()
{
	Image::draw(); //draw the ship first

	if( shieldOn ) //if the shield is on, draw it on top of the ship
	{
		shield.draw( spriteData, graphicsNS::ALPHA50 & colorFilter );
		//ship's spriteData -- shiled will have the same size and rotation of the ship
	}

}

void Ship::damage( WEAPON weapon )
{
	shieldOn = true;
}