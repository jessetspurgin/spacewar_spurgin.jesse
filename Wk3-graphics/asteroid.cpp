// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 version 1.0

#include "asteroid.h"

//=============================================================================
// default constructor
//=============================================================================
Asteroid::Asteroid() : Entity()
{
    spriteData.x    = asteroidNS::X;              // location on screen
    spriteData.y    = rand() % (GAME_HEIGHT - asteroidNS::HEIGHT);
    radius          = asteroidNS::COLLISION_RADIUS;
    mass            = asteroidNS::MASS;
    //startFrame      = asteroidNS::START_FRAME;    // first frame of ship animation
    //endFrame        = asteroidNS::END_FRAME;      // last frame of ship animation
    //setCurrentFrame(startFrame);
	respawn_timer = 0;
	waiting = false; 
	explode = false;                // turn off explosion
    visible = true;
	active = true;
}

void Asteroid::update( float frameTime ) 
{
	Entity::update ( frameTime );

	if( active ) 
	{
		spriteData.angle += frameTime * asteroidNS::ROTATION_RATE; // Rotate the ship
	
		spriteData.x += frameTime * velocity.x; // Move ship along X
		spriteData.y += frameTime * velocity.y; // Move ship along Y
	}

	// check if offscreen
	// then call reset()
	if( spriteData.x < -asteroidNS::WIDTH ) 
	{
		active = false;
	}

	//respawn the astroid
	if( !active && !explode ) 
	{
		if( ! waiting ) reset();

		respawn_timer += frameTime;
		if( respawn_timer > asteroidNS::RESPAWN_IN )
		{
			waiting = false;
			active = true;
			setVisible(true);
		}
	}

	if(explode)
    {
        explosion.update(frameTime);
        if(explosion.getAnimationComplete())    // if explosion animation complete
        {
            explode = false;                // turn off explosion
            visible = false;
            explosion.setAnimationComplete(false);
            explosion.setCurrentFrame(asteroidNS::EXPLOSION_START_FRAME);
			reset();
        }
    }
}

void Asteroid::reset()
{
	spriteData.x    = (float)GAME_WIDTH;              // location on screen
    spriteData.y    = rand() % (GAME_HEIGHT - asteroidNS::HEIGHT);
	
	waiting = true;
	active = false;
	respawn_timer = 0;
}

bool Asteroid::initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM)
{
	explosion.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
    explosion.setFrames(asteroidNS::EXPLOSION_START_FRAME, asteroidNS::EXPLOSION_END_FRAME);
    explosion.setCurrentFrame(asteroidNS::EXPLOSION_START_FRAME);
    explosion.setFrameDelay(asteroidNS::EXPLOSION_ANIMATION_DELAY);
    explosion.setLoop(false);               // do not loop animation
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

void Asteroid::draw()
{
    
	if(explode)
        explosion.draw(spriteData); // draw explosion using current spriteData
	else	
    {
        Image::draw();              // draw asteroid
    }
}
