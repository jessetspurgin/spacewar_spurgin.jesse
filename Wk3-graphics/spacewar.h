// 4.4 The Space War Class
// Chapter 4 page 103

#ifndef _SPACEWAR_H // Prevent multiple definitions if this
#define _SPACEWAR_H // file is included in more than one place
#define WIN32_LEAN_AND_MEAN
#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "ship.h"
#include "planet.h"
#include "asteroid.h"


// Spacewar is the class we create; it inherits from the Game class
class Spacewar : public Game
{
private:
	// Variables

	TextureManager nebulaTexture; //nebula image
//	TextureManager planetTexture; //planet texture
//	TextureManager shipTexture; //ship texture
	TextureManager gameTextures;
	TextureManager spaceTexture;

	Image nebula; //nebula image
	Image space;
	Planet planet; //planet image
	Ship ship1; //ship 1
	Ship ship2; //ship 2
	Asteroid asteroid1;
	Asteroid asteroid2;
	Asteroid asteroid3;

public:
	// Constructor
	Spacewar();
	// Destructor
	virtual ~Spacewar();
	// Initialize the game
	void initialize(HWND hwnd);
	void update(); // Must override pure virtual from Game
	void ai(); // "
	void collisions(); // "
	void render(); // "
	void releaseAll();
	void resetAll();
};
#endif