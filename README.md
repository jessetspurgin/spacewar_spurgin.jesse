course: GP 1111 - Coding for Games 1
assignment: Final Project - SpaceWar
due-date: 6/17/2015

author: Jesse Spurgin

creation-date: 4/29/2015

file: SpaceWar_spurgin.jesse

brief:

Move the player ship with:

W = Up
S = Down
A = Left
D = Right

Rotate the player ship with:

Q = Rotate Left
E = Rotate Right

Object is to destroy the asteroids before they reach the left side of the screen by crashing into them. Don't worry your Shields with protect you.

Obstacles are the planet and the enemy ship.

The score will increase for every asteroid you destroy.

The game has time limit, and Game Over, highest score wins.

Use the Console "~ key" to exit the game.